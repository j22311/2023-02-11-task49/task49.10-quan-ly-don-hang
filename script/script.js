$(document).ready(function(){
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gREQUEST_STATUS_OK = 200;
    const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;
    const gREQUEST_CREATE_SUCCESS = 201; // status 201 - tạo thành công
console.log("hello");
onPageLoading();

var gCombo = {
    kichCo: "",
    duongKinh: 0,
    suon: 0,
    salad: 0,
    soLuongNuoc: 0,
    thanhTien: 0
}

var gPizzaType = null;
var gMaDoUong = 0;
// var gVoucherId = 0;
var gPhanTramGiamGia = 0;
var gTaodon ;
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
 //on click to Button Gui don
 $("#form-gui-don").on("submit",function(event){
    event.preventDefault();
    onBtnGuiDon();
})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */  
//loading Page at First
function onPageLoading(){
    callAjaxDrinkList();
    onBtnGetAllOrderClick();
    // callAjaxcheckIdVoucher();
} 
//change green button small click
    $("#btn-small").on("click",function(){
        
        
        changeToGreenColorButton(this); 
        ChangeToYellowColorButton("#btn-medium");
        ChangeToYellowColorButton("#btn-large");           
        console.log("changeColor")
        getComboPizza("S","20","2","200","2","150000");
        console.log(gCombo);
    })
    //change green button smedium click
    $("#btn-medium").on("click",function(){
        
        
        changeToGreenColorButton(this); 
        ChangeToYellowColorButton("#btn-small");
        ChangeToYellowColorButton("#btn-large");           
        console.log("changeColor")
        getComboPizza("M","25","4","300","3","200000");
        console.log(gCombo);
    })
    //change green button large click
    $("#btn-large").on("click",function(){
        
        
        changeToGreenColorButton(this); 
        ChangeToYellowColorButton("#btn-small");
        ChangeToYellowColorButton("#btn-medium");           
        console.log("changeColor")
        getComboPizza("L","30","28","500","4","250000");
        console.log(gCombo);
    })
    //change green button seafood click
    $("#btn-seafood").on("click",function(){
        
        
        changeToGreenColorButton(this); 
        ChangeToYellowColorButton("#btn-hawaii");
        ChangeToYellowColorButton("#btn-bacon");           
        console.log("changeColor")
        gPizzaType = "SEAFOOD"
    })
     //change green button hawaii click
     $("#btn-hawaii").on("click",function(){
        
        
        changeToGreenColorButton(this); 
        ChangeToYellowColorButton("#btn-seafood");
        ChangeToYellowColorButton("#btn-bacon");           
        console.log("changeColor")
        gPizzaType = "HAWAII";
    })

    //change green button bacon click
    $("#btn-bacon").on("click",function(){
        
        
        changeToGreenColorButton(this); 
        ChangeToYellowColorButton("#btn-seafood");
        ChangeToYellowColorButton("#btn-hawaii");           
        console.log("changeColor")
        gPizzaType = "BACON";
        console.log(gPizzaType);
    })


    //on change choose select Drinks
    $("#slt-do-uong").on("change",function(){
        gMaDoUong = $(this).val();
        console.log(gMaDoUong);
    })

    // On Click button Gui don 
    function onBtnGuiDon(){
        var vObjectRequest = {
            kichCo: "",
            duongKinh: "",
            suon: "",
            // giamGia: 0,
            salad: "",
            loaiPizza: "",
            idVourcher: 0,
            idLoaiNuocUong: "",
            soLuongNuoc: "",
            hoTen: "",
            thanhTien: "",
            email: "",
            soDienThoai: "",
            diaChi: "",
            loiNhan: ""
        }

         getNewOrder(vObjectRequest);
         gPhanTramGiamGia = 0;
        if(!validate(vObjectRequest)){return;}
        console.log("bắt đầu tạo order:");
        console.log(vObjectRequest);
       // callAjaxcheckIdVoucher(vObjectRequest);
       onBtnCheckVoucherIdClick(vObjectRequest);
        console.log("phần trăm giảm giá tìm được:" + gPhanTramGiamGia);
       
    
         $("#modal-order").modal("show");
        
        
        loadInfoToModalOrder(vObjectRequest);

    }

    //on click button Tao Don
    $("#btn-tao-don").on("click",function(){
        onBtnCreateOrderClick();
    })
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
//FUNCTION change color button size Pizza


    //function CHange Green color Button
    function changeToGreenColorButton(paraButton){
        $(paraButton).attr("data-is-selected","Y");
        $(paraButton).removeClass("btn-warning");
        $(paraButton).addClass("btn-success");
    }
    //function CHange Yellow color Button
    function ChangeToYellowColorButton(paraButton){
        $(paraButton).attr("data-is-selected","N");
        $(paraButton).removeClass("btn-success");
        $(paraButton).addClass("btn-warning");
    }

    //mã nguồn để load data drink list (danh sách loại nước uống) về
    function onBtnGetDrinkListClick() {
        "use strict";
        const vBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/drinks";
        var vXhttp = new XMLHttpRequest();
        vXhttp.onreadystatechange =
            function () {
                if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
                    console.log(vXhttp.responseText); //ghi response text ra console.log
                }
            };
            vXhttp.open("GET",vBASE_URL, true);
            vXhttp.send();
    }
    //call ajax drink list
    function callAjaxDrinkList(){
        const vBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/drinks";
        $.ajax({
            url: vBASE_URL,
            type: "GET",
            dataType: 'json',
            success: function(responseObject){
            loadDrinksToSelectElement(responseObject);
            console.log(responseObject);
            },
            error: function(error){
            console.assert(error.responseText);
            }
        });
    }
    //load List drink to select
    function loadDrinksToSelectElement(paraList){
        var vSelect = $("#slt-do-uong");
        for(var i =0 ; i< paraList.length;i++){
            var vOption = $("<option>",{
                value: paraList[i].maNuocUong,
                text: paraList[i].tenNuocUong
            }).appendTo(vSelect);
        }
    }
     // lấy danh sách order
     function onBtnGetAllOrderClick() {
        "use strict";
       

        const vBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";
        $.ajax({
            url: vBASE_URL,
            type: "GET",
            dataType: 'json',
            success: function(responseObject){
            // loadDrinksToSelectElement(responseObject);
            console.log(responseObject);
            },
            error: function(error){
            console.assert(error.responseText);
            }
        });
    }

    
    //check mã giảm giá
    function onBtnCheckVoucherIdClick(paraObj) {
        "use strict";
        const vBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail";
        var vVoucherId = paraObj.idVourcher;  //một số mã đúng để test: 95531, 81432,...lưu ý test cả mã sai
        // nếu mã giảm giấ đã nhập, tạo xmlHttp  request và gửi về server
        var vXmlHttp = new XMLHttpRequest();
        vXmlHttp.open("GET", vBASE_URL + "/" + vVoucherId, false);
        vXmlHttp.send(null);
        var vStatusCode = vXmlHttp.status;
        if (vStatusCode == gREQUEST_STATUS_OK) { // restFullAPI successful
            // nhận lại response dạng JSON ở xmlHttp.responseText và chuyển thành object
            console.log(vXmlHttp.responseText);
            var vVOucherFound =JSON.parse(vXmlHttp.responseText);
            console.log("mã giảm giá tìm được");
            console.log(vVOucherFound);
            gPhanTramGiamGia = vVOucherFound.phanTramGiamGia;
        }
        else {
            // không nhận lại được data do vấn đề gì đó: khả năng mã voucher không dúng
            console.log("Không tìm thấy voucher " + vVoucherId);
        }
    }

    function callAjaxcheckIdVoucher(paraObj){
        "use strict";
      
        const vBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail";
        var vVoucherId = paraObj.idVourcher; 
        const vAPI_ = vBASE_URL + "/" + vVoucherId;
        $.ajax({
            url: vBASE_URL + "/" + paraObj.idVourcher,
            type: "GET",
            dataType: 'json',
            success: function(responseObject){
            console.log("mã giảm giá tìm được");
            console.log(responseObject);
            gPhanTramGiamGia = responseObject.phanTramGiamGia;
            
            
            
            },
            error: function(error){
            console.assert(error.responseText);
            }
        });
    }
    //function get combo Pizza
    function getComboPizza(paraKichCo,paraDuongKinh,
        paraSuonNuong,paraSalad,paraNuocNgot,paraThanhTien){
            gCombo.kichCo = paraKichCo;
            gCombo.duongKinh = paraDuongKinh;
            gCombo.suon = paraSuonNuong;
            gCombo.salad = paraSalad;
            gCombo.soLuongNuoc = paraNuocNgot;
            gCombo.thanhTien = paraThanhTien;
    }

    //create a new order
    function getNewOrder(paraObj){
        paraObj.kichCo = gCombo.kichCo;
        paraObj.duongKinh = gCombo.duongKinh;
        paraObj.suon = gCombo.suon;
        paraObj.salad = gCombo.salad;
        paraObj.soLuongNuoc = gCombo.soLuongNuoc;
        paraObj.thanhTien = gCombo.thanhTien;
       
        paraObj.loaiPizza = gPizzaType;

        paraObj.idLoaiNuocUong = gMaDoUong;

        paraObj.hoTen = $("#inp-name").val().trim();
        paraObj.email = $("#inp-email").val().trim();
        paraObj.soDienThoai = $("#inp-phone-number").val().trim();
        paraObj.diaChi = $("#inp-adrress").val().trim();
        paraObj.idVourcher = $("#inp-vouher-discount").val().trim();
        paraObj.loiNhan = $("#inp-messenger").val().trim();
        
    }

    //validate obj 
    function validate(paraObj){
        if(paraObj.kichCo == ""){
            alert("you didnt choose Size yet");
            return false;
        }

        if(paraObj.loaiPizza == null){
            alert("you didnt choose pizza type yet");
            return false;
        }

        if(paraObj.idLoaiNuocUong == 0){
            alert("you didnt choose drink type yet");
            return false;
        }

        if(paraObj.hoTen == "" || paraObj.soDienThoai == "" ||
        paraObj.diaChi == "" ){
            alert("invalide infomation");
            return false;
        }
        return true;


    }

    //load info to modal order
    function loadInfoToModalOrder(paraObj){
        $("#inp-name-modal").val( paraObj.hoTen);
        
        $("#inp-phone-number-modal").val(paraObj.soDienThoai );
        $("#inp-adrress-modal").val(paraObj.diaChi);
        $("#inp-vouher-discount-modal").val(paraObj.idVourcher );
        $("#inp-messenger-modal").val(paraObj.loiNhan);
        debugger;
        var vTinhTien = tinhTien(paraObj.thanhTien,gPhanTramGiamGia);
        // cập nhật thành tiền 
        
        $("#text-info").val("xác nhận: " + paraObj.hoTen + ", " 
        + paraObj.diaChi + "\n Menu " + paraObj.kichCo + ", sườn nướng: " + paraObj.suon + 
        " nước: " + paraObj.soLuongNuoc + "\n loại Pizza: " + paraObj.loaiPizza +
        " Giá: " + paraObj.thanhTien +" VND, Mã giảm giá: " + paraObj.idVourcher +
        "\n phải thanh toán: " + vTinhTien +
          " VND, (giảm giá: "+ gPhanTramGiamGia +  " %)");
        

        paraObj.thanhTien = vTinhTien;
        gTaodon = paraObj;

    }

    //function tính tiền cho pizza
    function tinhTien(paraThanhTien,paraGiamGia){
        var vResult = 0;
        console.log("phan tram giam gia:" + paraGiamGia);
        vResult = paraThanhTien - paraThanhTien*paraGiamGia/100;
        debugger;
        return vResult;
    }

     // tạo một order mới
     function onBtnCreateOrderClick() {
        "use strict";
        //base url
        const vBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";
       
        var vObjectRequest = gTaodon;

        var vXmlHttpCreateOrder = new XMLHttpRequest();
        vXmlHttpCreateOrder.open("POST", vBASE_URL, true);
        vXmlHttpCreateOrder.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        vXmlHttpCreateOrder.send(JSON.stringify(vObjectRequest));
        vXmlHttpCreateOrder.onreadystatechange =
            function () {
                if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_CREATE_SUCCESS) { // status 201 tao thanh cong
                    var vCreatedOrder = JSON.parse(vXmlHttpCreateOrder.responseText);
                    console.log(vCreatedOrder);
                    handleModalAfterCreateOrder(vCreatedOrder);
                }
            }
    }

    //handle modal after creat an order success
    function handleModalAfterCreateOrder(paraObj){
        alert("tạo đơn thành công");
        resetPage();
        $("#modal-order").modal("hide");
        $("#modal-finish").modal("show")
        $("#inp-ma-don-hang").val(paraObj.orderCode);
    }
    
    //reset all page
    function resetPage(){
        ChangeToYellowColorButton("#btn-medium");
        ChangeToYellowColorButton("#btn-large"); 
        ChangeToYellowColorButton("#btn-small");   

        ChangeToYellowColorButton("#btn-hawaii");
        ChangeToYellowColorButton("#btn-bacon");  
        ChangeToYellowColorButton("#btn-seafood");
        getComboPizza("","0","0","0","0","0");
        gPizzaType = null;
        gMaDoUong = 0;

        $("#slt-do-uong").val(0);

       $("#inp-name").val("");
        $("#inp-email").val("");
        $("#inp-phone-number").val("");
       $("#inp-adrress").val();
       $("#inp-vouher-discount").val("");
       $("#inp-messenger").val("");
    }
})